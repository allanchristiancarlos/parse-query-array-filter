<?php

namespace Korvipe\ParseQueryArrayFilter;

use Parse\ParseQuery;

class Factory
{
    public static function create(ParseQuery $query, array $filters)
    {
        $builder = new QueryBuilder($query, $filters);
        return $builder->getQuery();
    }
}