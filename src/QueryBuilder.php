<?php

namespace Korvipe\ParseQueryArrayFilter;

use Parse\ParseQuery;
use Parse\ParseObject;

class QueryBuilder
{
    protected $query;
    protected $filters;

    public function __construct(ParseQuery $query, array $filters)
    {
        $this->query = $query;
        $this->filters = $filters;

        $this->assignFilters();
    }

    public function getQuery()
    {
        return $this->query;
    }

    private function assignFilters()
    {
        foreach ($this->filters as $key => $filter) {
            if (is_array($filter)) {
                $type    = isset($filter['type']) ? $filter['type'] : 'string';
                $compare = isset($filter['compare']) ? $filter['compare'] : '';
                $value   = $filter['value'];
                $column  = $filter['column'];

                if ($type == 'pointer') {
                    $value = new ParseObject($filter['class'], $value);
                }

                switch ($compare) {
                    case 'contains_all':
                        $this->query->containsAll($column, $value);
                        break;
                    case 'contained_in':
                        $this->query->containedIn($column, $value);
                        break;
                    case 'not_contained_in':
                        $this->query->notContainedIn($column, $value);
                        break;
                    case 'ends_with':
                        $this->query->endsWith($column, $value);
                        break;
                    case 'starts_with':
                        $this->query->startsWith($column, $value);
                        break;
                    case 'less_than_or_equal_to':
                        $this->query->lessThanOrEqualTo($column, $value);
                        break;
                    case 'greater_than_or_equal_to':
                        $this->query->greaterThanOrEqualTo($column, $value);
                        break;
                    case 'greater_than':
                        $this->query->greaterThan($column, $value);
                        break;
                    case 'less_than':
                        $this->query->lessThan($column, $value);
                        break;
                    case 'not_equal_to':
                        $this->query->notEqualTo($column, $value);
                        break;
                    default:
                        $this->query->equalTo($column, $value);
                }
            } else {
                // This looks like this
                // myColumn=someValue
                $this->query->equalTo($key, $filter);
            }
        }
    }
}